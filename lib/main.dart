import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Center(
              child: Text("MoreCare App"),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Text(
                  "การแจ้งเตือน",
                  style: TextStyle(fontSize: 24),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      //color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  height: 150,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "09.00 น.  \nวันนี้ \n31 ม.ค. 65  ",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: 6,
                        height: 100,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(175, 242, 242, 1),
                        ),
                      ),
                      Text("   "),
                      Container(
                        width: 12,
                        height: 12,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(248, 173, 173, 1),
                          borderRadius:
                              BorderRadius.all(Radius.elliptical(12, 12)),
                        ),
                      ),
                      Text(
                        "  ทานยาก่อนอาหารเช้า ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      // color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  height: 150,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "13.00 น.  \nวันนี้ \n31 ม.ค. 65  ",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: 6,
                        height: 100,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(175, 242, 242, 1),
                        ),
                      ),
                      Text("   "),
                      Container(
                        width: 12,
                        height: 12,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(248, 173, 173, 1),
                          borderRadius:
                              BorderRadius.all(Radius.elliptical(12, 12)),
                        ),
                      ),
                      Text(
                        "  ทานยาหลังอาหารกลางวัน ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      // color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  height: 150,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "17.00 น.  \nวันนี้ \n31 ม.ค. 65  ",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: 6,
                        height: 100,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(175, 242, 242, 1),
                        ),
                      ),
                      Text("   "),
                      Container(
                        width: 12,
                        height: 12,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(248, 173, 173, 1),
                          borderRadius:
                              BorderRadius.all(Radius.elliptical(12, 12)),
                        ),
                      ),
                      Text(
                        "  ทานยาก่อนอาหารเย็น ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      // color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  height: 150,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "22.00 น.  \nวันนี้ \n31 ม.ค. 65  ",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: 6,
                        height: 100,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(175, 242, 242, 1),
                        ),
                      ),
                      Text("   "),
                      Container(
                        width: 12,
                        height: 12,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(248, 173, 173, 1),
                          borderRadius:
                              BorderRadius.all(Radius.elliptical(12, 12)),
                        ),
                      ),
                      Text(
                        "  ทานยาก่อนนอน ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
